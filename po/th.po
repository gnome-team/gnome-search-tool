# Thai translation for gnome-utils.
# Copyright (C) 2004-2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-utils package.
# Khunti Yeesoon <sc445138@bucc3.buu.ac.th>, 2003.
# Paisa Seeluangsawat <paisa@users.sf.net>, 2004.
# Supranee Thirawatthanasuk <supranee@opentle.org>, 2004.
# Theppitak Karoonboonyanan <thep@linux.thai.net>, 2005-2012.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-utils\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?product=gnome-"
"search-tool&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-08-06 17:31+0000\n"
"PO-Revision-Date: 2012-09-07 16:20+0700\n"
"Last-Translator: Theppitak Karoonboonyanan <thep@linux.thai.net>\n"
"Language-Team: Thai <thai-l10n@googlegroups.com>\n"
"Language: th\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../data/gnome-search-tool.desktop.in.h:1
msgid "Search for Files..."
msgstr "ค้นหาแฟ้ม..."

#: ../data/gnome-search-tool.desktop.in.h:2
msgid "Locate documents and folders on this computer by name or content"
msgstr "ค้นหาเอกสารและโฟลเดอร์ในคอมพิวเตอร์นี้ด้วยชื่อหรือเนื้อหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:1
msgid "Search history"
msgstr "ประวัติการค้น"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:2
msgid "This key defines the items which were searched for in the past."
msgstr "เก็บรายการการสืบค้นต่างๆ ในอดีต"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:3
msgid "Show Additional Options"
msgstr "แสดงตัวเลือกอื่นๆ"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:4
msgid "Disable Quick Search"
msgstr "ปิดการค้นหาแบบเร็ว"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:5
msgid ""
"This key determines if the search tool disables the use of the locate "
"command when performing simple file name searches."
msgstr "กำหนดให้เครื่องมือค้นหาปิดการใช้คำสั่ง locate สำหรับการค้นหาชื่อแฟ้มอย่างง่าย"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:6
msgid "Quick Search Excluded Paths"
msgstr "พาธที่ไม่รวมในการค้นอย่างเร็ว"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:7
msgid ""
"This key defines the paths the search tool will exclude from a quick search. "
"The wildcards '*' and '?' are supported. The default values are /mnt/*, /"
"media/*, /dev/*, /tmp/*, /proc/*, and /var/*."
msgstr ""
"กำหนดพาธที่เครื่องมือค้นหาจะไม่รวมในผลลัพธ์ของการค้นอย่างเร็ว สามารถใช้ wildcard '*' และ "
"'?' ได้ ค่าโดยปริยายคือ /mnt/*, /media/*, /dev/*, /tmp/*, /proc/*, และ /var/*"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:8
msgid "Disable Quick Search Second Scan"
msgstr "ปิดการค้นรอบสองของการค้าหาแบบเร็ว"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:9
msgid ""
"This key determines if the search tool disables the use of the find command "
"after performing a quick search."
msgstr "กำหนดให้เครื่องมือค้นหาปิดการใช้คำสั่ง find หลังจากค้นหาอย่างเร็วเสร็จแล้ว"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:10
msgid "Quick Search Second Scan Excluded Paths"
msgstr "พาธที่ไม่รวมในการค้นอย่างเร็วรอบสอง"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:11
msgid ""
"This key defines the paths the search tool will exclude from a second scan "
"when performing a quick search. The second scan uses the find command to "
"search for files. The purpose of the second scan is to find files that have "
"not been indexed. The wildcards '*' and '?' are supported. The default value "
"is /."
msgstr ""
"กำหนดพาธที่เครื่องมือค้นหาจะไม่ค้นในรอบสองของการค้นอย่างเร็ว การค้นรอบสองจะใช้คำสั่ง find "
"ในการค้นหาแฟ้ม มีจุดมุ่งหมายเพื่อค้นหาแฟ้มที่ยังไม่ถูกทำดัชนี สามารถใช้ wildcard '*' และ '?' "
"ได้ ค่าโดยปริยายคือ /"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:12
msgid "Search Result Columns Order"
msgstr "ลำดับคอลัมน์ในการแสดงผลการค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:13
msgid ""
"This key defines the order of the columns in the search results. This key "
"should not be modified by the user."
msgstr "กำหนดลำดับของคอลัมน์ในการแสดงผลการค้นหา ผู้ใช้ไม่ควรเปลี่ยนแปลงคีย์นี้"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:14
msgid "Default Window Width"
msgstr "ความกว้างหน้าต่างปริยาย"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:15
msgid ""
"This key defines the window width, and it's used to remember the size of the "
"search tool between sessions. Setting it to -1 will make the search tool use "
"the default width."
msgstr ""
"กำหนดความกว้างของหน้าต่าง และใช้จำขนาดของเครื่องมือค้นหาข้ามวาระ ถ้ากำหนดเป็น -1 "
"จะใช้ความกว้างปริยาย"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:16
msgid "Default Window Height"
msgstr "ความสูงหน้าต่างปริยาย"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:17
msgid ""
"This key defines the window height, and it's used to remember the size of "
"the search tool between sessions. Setting it to -1 will make the search tool "
"use the default height."
msgstr ""
"กำหนดความสูงของหน้าต่าง และใช้จำขนาดของเครื่องมือค้นหาข้ามวาระ ถ้ากำหนดเป็น -1 "
"จะใช้ความสูงปริยาย"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:18
msgid "Default Window Maximized"
msgstr "หน้าต่างขยายเต็มโดยปริยาย"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:19
msgid ""
"This key determines if the search tool window starts in a maximized state."
msgstr "กำหนดว่าเครื่องมือค้นหาจะเริ่มทำงานแบบขยายหน้าต่างเต็มหรือไม่"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:20
msgid "Look in Folder"
msgstr "หาในโฟลเดอร์"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:21
msgid "This key defines the default value of the \"Look in Folder\" widget."
msgstr "กำหนดค่าปริยายของวิดเจ็ต \"หาในโฟลเดอร์\""

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:22
msgid ""
"This key determines if the \"Contains the text\" search option is selected "
"when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"มีข้อความ\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:23
msgid ""
"This key determines if the \"Date modified less than\" search option is "
"selected when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"แก้ไขครั้งล่าสุดภายใน\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:24
msgid ""
"This key determines if the \"Date modified more than\" search option is "
"selected when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"แก้ไขครั้งล่าสุดก่อน\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:25
msgid ""
"This key determines if the \"Size at least\" search option is selected when "
"the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"ขนาดอย่างน้อย\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:26
msgid ""
"This key determines if the \"Size at most\" search option is selected when "
"the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"ขนาดไม่เกิน\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:27
msgid ""
"This key determines if the \"File is empty\" search option is selected when "
"the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"แฟ้มเปล่าไม่มีข้อมูล\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:28
msgid ""
"This key determines if the \"Owned by user\" search option is selected when "
"the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"เจ้าของคือ\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:29
msgid ""
"This key determines if the \"Owned by group\" search option is selected when "
"the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"เป็นของกลุ่ม\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:30
msgid ""
"This key determines if the \"Owner is unrecognized\" search option is "
"selected when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"ไม่รู้จักเจ้าของ\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:31
msgid ""
"This key determines if the \"Name does not contain\" search option is "
"selected when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"ชื่อไม่มีคำว่า\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:32
msgid ""
"This key determines if the \"Name matches regular expression\" search option "
"is selected when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"ชื่อที่ตรงกับนิพจน์เรกิวลาร์\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:33
msgid ""
"This key determines if the \"Show hidden files and folders\" search option "
"is selected when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"แสดงแฟ้มสำรอง และแฟ้มที่ถูกซ่อน\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:34
msgid ""
"This key determines if the \"Follow symbolic links\" search option is "
"selected when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"ติดตามลิงก์สัญลักษณ์\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../data/org.gnome.gnome-search-tool.gschema.xml.in.h:35
msgid ""
"This key determines if the \"Exclude other filesystems\" search option is "
"selected when the search tool is started."
msgstr "กำหนดให้เลือกตัวเลือก \"ไม่รวมระบบแฟ้มอื่น\" เมื่อเริ่มเปิดเครื่องมือค้นหา"

#: ../src/gsearchtool.c:81
msgid "Contains the _text"
msgstr "_มีข้อความ"

#: ../src/gsearchtool.c:83
msgid "_Date modified less than"
msgstr "แก้ไขครั้งล่าสุด_ภายใน"

#: ../src/gsearchtool.c:83 ../src/gsearchtool.c:84
msgid "days"
msgstr "วัน"

#: ../src/gsearchtool.c:84
msgid "Date modified more than"
msgstr "แก้ไขครั้งล่าสุด_ก่อน"

#: ../src/gsearchtool.c:86
msgid "S_ize at least"
msgstr "ขนาดอ_ย่างน้อย"

#: ../src/gsearchtool.c:86 ../src/gsearchtool.c:87
msgid "kilobytes"
msgstr "กิโลไบต์"

#: ../src/gsearchtool.c:87
msgid "Si_ze at most"
msgstr "ขนาดไ_ม่เกิน"

#: ../src/gsearchtool.c:88
msgid "File is empty"
msgstr "แฟ้มเปล่าไม่มีข้อมูล"

#: ../src/gsearchtool.c:90
msgid "Owned by _user"
msgstr "เ_จ้าของคือ"

#: ../src/gsearchtool.c:91
msgid "Owned by _group"
msgstr "เป็น_ของกลุ่ม"

#: ../src/gsearchtool.c:92
msgid "Owner is unrecognized"
msgstr "ไม่รู้จักเจ้าของ"

#: ../src/gsearchtool.c:94
msgid "Na_me does not contain"
msgstr "ชื่อไม่มี_คำว่า"

#: ../src/gsearchtool.c:95
msgid "Name matches regular e_xpression"
msgstr "ชื่อที่ตร_งกับนิพจน์เรกิวลาร์"

#: ../src/gsearchtool.c:97
msgid "Show hidden and backup files"
msgstr "แสดงแฟ้มสำรอง และแฟ้มที่ถูกซ่อน"

# Once created, hard link is a normal file.
# จุดเชื่อม here unambiguously means symbolic link.
#: ../src/gsearchtool.c:98
msgid "Follow symbolic links"
msgstr "ติดตามลิงก์สัญลักษณ์"

#: ../src/gsearchtool.c:99
msgid "Exclude other filesystems"
msgstr "ไม่รวมระบบแฟ้มอื่น"

#: ../src/gsearchtool.c:157
msgid "Show version of the application"
msgstr "แสดงเลขรุ่นของโปรแกรม"

#: ../src/gsearchtool.c:158 ../src/gsearchtool.c:163 ../src/gsearchtool.c:172
msgid "STRING"
msgstr "STRING"

#: ../src/gsearchtool.c:159
msgid "PATH"
msgstr "PATH"

#: ../src/gsearchtool.c:160
msgid "VALUE"
msgstr "VALUE"

#: ../src/gsearchtool.c:164 ../src/gsearchtool.c:165
msgid "DAYS"
msgstr "DAYS"

#: ../src/gsearchtool.c:166 ../src/gsearchtool.c:167
msgid "KILOBYTES"
msgstr "KILOBYTES"

#: ../src/gsearchtool.c:169
msgid "USER"
msgstr "USER"

#: ../src/gsearchtool.c:170
msgid "GROUP"
msgstr "GROUP"

#: ../src/gsearchtool.c:173
msgid "PATTERN"
msgstr "PATTERN"

#: ../src/gsearchtool.c:384
msgid "A locate database has probably not been created."
msgstr "เป็นไปได้ว่าระบบยังไม่ได้สร้างฐานข้อมูล locate"

#: ../src/gsearchtool.c:486
#, c-format
msgid "Character set conversion failed for \"%s\""
msgstr "ไม่สามารถแปลงรหัสอักขระของ \"%s\""

#: ../src/gsearchtool.c:506
msgid "Searching..."
msgstr "กำลังค้นหา..."

#: ../src/gsearchtool.c:506 ../src/gsearchtool.c:1019
#: ../src/gsearchtool.c:2987
msgid "Search for Files"
msgstr "ค้นหาแฟ้ม"

#: ../src/gsearchtool.c:965 ../src/gsearchtool.c:994
msgid "No files found"
msgstr "ไม่พบแฟ้ม"

#: ../src/gsearchtool.c:987
msgid "(stopped)"
msgstr "(หยุด)"

#: ../src/gsearchtool.c:993
msgid "No Files Found"
msgstr "ไม่พบแฟ้ม"

#: ../src/gsearchtool.c:998
#, c-format
msgid "%'d File Found"
msgid_plural "%'d Files Found"
msgstr[0] "พบ %'d แฟ้ม"

#: ../src/gsearchtool.c:1002 ../src/gsearchtool.c:1040
#, c-format
msgid "%'d file found"
msgid_plural "%'d files found"
msgstr[0] "พบ %'d แฟ้ม"

#: ../src/gsearchtool.c:1131
msgid "Entry changed called for a non entry option!"
msgstr "รายการที่แก้ไขต้องได้รับค่าที่ไม่ว่างเปล่า"

#: ../src/gsearchtool.c:1296
msgid "Set the text of \"Name contains\" search option"
msgstr "ตั้งข้อความสำหรับตัวเลือก \"ชื่อมีคำว่า\""

#: ../src/gsearchtool.c:1297
msgid "Set the text of \"Look in folder\" search option"
msgstr "ตั้งข้อความสำหรับตัวเลือก \"หาในโฟลเดอร์\""

#: ../src/gsearchtool.c:1298
msgid "Sort files by one of the following: name, folder, size, type, or date"
msgstr "เรียงแฟ้มตาม: ชื่อ โฟลเดอร์ ขนาด ชนิด หรือวัน"

#: ../src/gsearchtool.c:1299
msgid "Set sort order to descending, the default is ascending"
msgstr "ใช้การเรียงจากมากไปน้อย  ค่าปริยายคือน้อยไปมาก"

#: ../src/gsearchtool.c:1300
msgid "Automatically start a search"
msgstr "เริ่มการค้นหาโดยอัตโนมัติ"

#: ../src/gsearchtool.c:1306
#, c-format
msgid "Select the \"%s\" search option"
msgstr "เลือกตัวเลือก '%s'"

#: ../src/gsearchtool.c:1309
#, c-format
msgid "Select and set the \"%s\" search option"
msgstr "เลือกและตั้งค่าตัวเลือก \"%s\""

#: ../src/gsearchtool.c:1416
msgid "Invalid option passed to sortby command line argument."
msgstr "คำสั่ง sortby ได้รับตัวเลือกที่ใช้ไม่ได้"

#: ../src/gsearchtool.c:1710
msgid ""
"\n"
"... Too many errors to display ..."
msgstr ""
"\n"
"... ข้อผิดพลาดเยอะเกินกว่าจะแสดงได้หมด ..."

#: ../src/gsearchtool.c:1724
msgid ""
"The search results may be invalid.  There were errors while performing this "
"search."
msgstr "ผลการค้นหาอาจไม่ถูกต้อง  เกิดข้อผิดพลาดขณะค้นหา"

#: ../src/gsearchtool.c:1733 ../src/gsearchtool.c:1774
msgid "Show more _details"
msgstr "แ_สดงรายละเอียดเพิ่มเติม"

#: ../src/gsearchtool.c:1763
msgid ""
"The search results may be out of date or invalid.  Do you want to disable "
"the quick search feature?"
msgstr "ผลการค้นหาอาจเป็นข้อมูลเก่าหรือไม่ถูกต้อง  คุณต้องการปิดการค้นหาแบบเร็วหรือไม่?"

#: ../src/gsearchtool.c:1785
msgid "Disable _Quick Search"
msgstr "ปิดการค้นหาแบบเ_ร็ว"

#: ../src/gsearchtool.c:1812
#, c-format
msgid "Failed to set process group id of child %d: %s.\n"
msgstr "ไม่สามารถกำหนดหมายเลขกลุ่มของโพรเซสลูกหมายเลข %d: %s\n"

#: ../src/gsearchtool.c:1837
msgid "Error parsing the search command."
msgstr "ไม่เข้าใจคำสั่งค้นหา"

#: ../src/gsearchtool.c:1866
msgid "Error running the search command."
msgstr "เกิดข้อผิดพลาดขณะเรียกใช้งานคำสั่งค้นหา"

#: ../src/gsearchtool.c:1982
#, c-format
msgid "Enter a text value for the \"%s\" search option."
msgstr "ป้อนข้อความสำหรับตัวเลือกการค้นหา \"%s\""

#. Translators:  Below is a string displaying the search options name
#. and unit value.  For example, "\"Date modified less than\" in days".
#: ../src/gsearchtool.c:1987
#, c-format
msgid "\"%s\" in %s"
msgstr "\"%s\" ในหน่วย %s"

#: ../src/gsearchtool.c:1989
#, c-format
msgid "Enter a value in %s for the \"%s\" search option."
msgstr "ป้อนค่าในหน่วย %s สำหรับตัวเลือกการค้นหา \"%s\""

#: ../src/gsearchtool.c:2047
#, c-format
msgid "Remove \"%s\""
msgstr "ลบตัวเลือก \"%s\""

#: ../src/gsearchtool.c:2048
#, c-format
msgid "Click to remove the \"%s\" search option."
msgstr "คลิกเพื่อลบตัวเลือกการค้นหา \"%s\""

#: ../src/gsearchtool.c:2141
msgid "A_vailable options:"
msgstr "_ตัวเลือกที่มี:"

#: ../src/gsearchtool.c:2170
msgid "Available options"
msgstr "ตัวเลือกที่มี"

#: ../src/gsearchtool.c:2171
msgid "Select a search option from the drop-down list."
msgstr "เลือกตัวเลือกการค้นหาจากเมนู"

#: ../src/gsearchtool.c:2183
msgid "Add search option"
msgstr "เพิ่มตัวเลือกการค้นหา"

#: ../src/gsearchtool.c:2184
msgid "Click to add the selected available search option."
msgstr "คลิกเพื่อเพิ่มตัวเลือกการค้นหาที่ได้เลือกไว้ในรายการ"

#: ../src/gsearchtool.c:2273
msgid "S_earch results:"
msgstr "_ผลการค้นหา:"

#: ../src/gsearchtool.c:2317
msgid "List View"
msgstr "รายชื่อแฟ้มที่พบ"

#: ../src/gsearchtool.c:2377
msgid "Name"
msgstr "ชื่อ"

#: ../src/gsearchtool.c:2401
msgid "Folder"
msgstr "โฟลเดอร์"

#: ../src/gsearchtool.c:2414
msgid "Size"
msgstr "ขนาด"

#: ../src/gsearchtool.c:2426
msgid "Type"
msgstr "ชนิด"

#: ../src/gsearchtool.c:2438
msgid "Date Modified"
msgstr "วันที่แก้ไข"

#: ../src/gsearchtool.c:2757
msgid "_Name contains:"
msgstr "_ชื่อมีคำว่า:"

#: ../src/gsearchtool.c:2771 ../src/gsearchtool.c:2772
msgid "Enter a filename or partial filename with or without wildcards."
msgstr "ป้อนชื่อแฟ้มหรือบางส่วนของชื่อแฟ้มที่จะค้นหา สามารถใช้ wildcard ได้"

#: ../src/gsearchtool.c:2772
msgid "Name contains"
msgstr "ชื่อมีคำว่า"

#: ../src/gsearchtool.c:2778
msgid "_Look in folder:"
msgstr "หาในโฟ_ลเดอร์:"

#: ../src/gsearchtool.c:2784
msgid "Browse"
msgstr "เรียกดู"

#: ../src/gsearchtool.c:2793
msgid "Look in folder"
msgstr "หาในโฟลเดอร์"

#: ../src/gsearchtool.c:2793
msgid "Select the folder or device from which you want to begin the search."
msgstr "ใส่ชื่อโฟลเดอร์หรืออุปกรณ์ที่จะใช้เริ่มค้นหา"

#: ../src/gsearchtool.c:2811
msgid "Select more _options"
msgstr "เลือกตัวเลือก_อื่นๆ"

#: ../src/gsearchtool.c:2820
msgid "Select more options"
msgstr "เลือกตัวเลือกอื่นๆ"

#: ../src/gsearchtool.c:2820
msgid "Click to expand or collapse the list of available options."
msgstr "คลิกเพื่อขยายหรือยุบรายการตัวเลือกของการค้นหา"

#: ../src/gsearchtool.c:2844
msgid "Click to display the help manual."
msgstr "คลิกเพื่อเปิดเอกสารแนะนำวิธีใช้"

#: ../src/gsearchtool.c:2852
msgid "Click to close \"Search for Files\"."
msgstr "คลิกถ้าต้องการปิดหน้าต่าง \"ค้นหาแฟ้ม\""

#: ../src/gsearchtool.c:2878
msgid "Click to perform a search."
msgstr "คลิกเพื่อเริ่มค้นหา"

#: ../src/gsearchtool.c:2879
msgid "Click to stop a search."
msgstr "คลิกถ้าต้องการหยุดค้นหา"

#: ../src/gsearchtool.c:2972
msgid "- the GNOME Search Tool"
msgstr "- เครื่องมือค้นหาของ GNOME"

#: ../src/gsearchtool.c:2981
#, c-format
msgid "Failed to parse command line arguments: %s\n"
msgstr "แจงอาร์กิวเมนต์บรรทัดคำสั่งไม่สำเร็จ: %s\n"

#: ../src/gsearchtool-callbacks.c:197
msgid "Could not open help document."
msgstr "ไม่สามารถเปิดเอกสารคำแนะนำช่วยเหลือ"

#: ../src/gsearchtool-callbacks.c:344
#, c-format
msgid "Are you sure you want to open %d document?"
msgid_plural "Are you sure you want to open %d documents?"
msgstr[0] "แน่ใจหรือไม่ว่าจะเปิดเอกสาร %d ฉบับ?"

#: ../src/gsearchtool-callbacks.c:349 ../src/gsearchtool-callbacks.c:534
#, c-format
msgid "This will open %d separate window."
msgid_plural "This will open %d separate windows."
msgstr[0] "จะเปิดหน้าต่างแยก %d หน้าต่าง"

#: ../src/gsearchtool-callbacks.c:385
#, c-format
msgid "Could not open document \"%s\"."
msgstr "ไม่สามารถเปิดเอกสาร \"%s\""

#: ../src/gsearchtool-callbacks.c:410
#, c-format
msgid "Could not open folder \"%s\"."
msgstr "ไม่สามารถเปิดโฟลเดอร์ \"%s\""

#: ../src/gsearchtool-callbacks.c:418
msgid "The nautilus file manager is not running."
msgstr "โปรแกรมจัดการแฟ้ม Nautilus ไม่ได้ทำงานอยู่"

#: ../src/gsearchtool-callbacks.c:491 ../src/gsearchtool-callbacks.c:814
msgid "The document does not exist."
msgstr "เอกสารไม่มีอยู่จริง"

#: ../src/gsearchtool-callbacks.c:506
msgid "There is no installed viewer capable of displaying the document."
msgstr "ไม่มีโปรแกรมที่สามารถแสดงเอกสารนี้ได้ติดตั้งอยู่"

#: ../src/gsearchtool-callbacks.c:529
#, c-format
msgid "Are you sure you want to open %d folder?"
msgid_plural "Are you sure you want to open %d folders?"
msgstr[0] "แน่ใจหรือไม่ว่าจะเปิดโฟลเดอร์ %d โฟลเดอร์?"

#: ../src/gsearchtool-callbacks.c:678
#, c-format
msgid "Could not move \"%s\" to trash."
msgstr "ไม่สามารถย้าย \"%s\" ไปลงถังขยะ"

#: ../src/gsearchtool-callbacks.c:705
#, c-format
msgid "Do you want to delete \"%s\" permanently?"
msgstr "ต้องการลบ \"%s\" ทิ้งถาวรเลยหรือไม่?"

#: ../src/gsearchtool-callbacks.c:708
#, c-format
msgid "Trash is unavailable.  Could not move \"%s\" to the trash."
msgstr "ถังขยะใช้งานไม่ได้ จึงไม่สามารถย้าย \"%s\" ไปลงถังขยะ"

#: ../src/gsearchtool-callbacks.c:743
#, c-format
msgid "Could not delete \"%s\"."
msgstr "ไม่สามารถลบ \"%s\""

#: ../src/gsearchtool-callbacks.c:850
#, c-format
msgid "Deleting \"%s\" failed: %s."
msgstr "ลบ \"%s\" ไม่สำเร็จ: %s"

#: ../src/gsearchtool-callbacks.c:862
#, c-format
msgid "Moving \"%s\" failed: %s."
msgstr "ย้าย \"%s\" ไม่สำเร็จ: %s"

#. Popup menu item: Open
#: ../src/gsearchtool-callbacks.c:990 ../src/gsearchtool-callbacks.c:1020
msgid "_Open"
msgstr "_เปิด"

#. Popup menu item: Open with (default)
#: ../src/gsearchtool-callbacks.c:1045
#, c-format
msgid "_Open with %s"
msgstr "_เปิดด้วย %s"

#: ../src/gsearchtool-callbacks.c:1080
#, c-format
msgid "Open with %s"
msgstr "เปิดด้วย %s"

#. Popup menu item: Open With
#: ../src/gsearchtool-callbacks.c:1113
msgid "Open Wit_h"
msgstr "เปิดด้ว_ย"

#. Popup menu item: Open Containing Folder
#: ../src/gsearchtool-callbacks.c:1158
msgid "Open Containing _Folder"
msgstr "เปิดโ_ฟลเดอร์ที่บรรจุ"

#: ../src/gsearchtool-callbacks.c:1176
msgid "Mo_ve to Trash"
msgstr "_ทิ้งลงถังขยะ"

#: ../src/gsearchtool-callbacks.c:1198
msgid "_Save Results As..."
msgstr "_บันทึกผลการค้นหาเป็น..."

#: ../src/gsearchtool-callbacks.c:1580
msgid "Save Search Results As..."
msgstr "บันทึกผลการค้นหาเป็น..."

#: ../src/gsearchtool-callbacks.c:1611
msgid "Could not save document."
msgstr "ไม่สามารถบันทึกเอกสาร"

#: ../src/gsearchtool-callbacks.c:1612
msgid "You did not select a document name."
msgstr "คุณยังไม่ได้เลือกชื่อเอกสาร"

#: ../src/gsearchtool-callbacks.c:1638
#, c-format
msgid "Could not save \"%s\" document to \"%s\"."
msgstr "ไม่สามารถบันทึกเอกสาร \"%s\" ไปยัง \"%s\""

#: ../src/gsearchtool-callbacks.c:1668
#, c-format
msgid "The document \"%s\" already exists.  Would you like to replace it?"
msgstr "มีเอกสารชื่อ \"%s\" อยู่แล้ว  จะเขียนทับหรือไม่?"

#: ../src/gsearchtool-callbacks.c:1672
msgid "If you replace an existing file, its contents will be overwritten."
msgstr "ถ้าคุณเขียนทับ ข้อมูลในแฟ้มเก่าจะสูญหาย"

#: ../src/gsearchtool-callbacks.c:1683
msgid "_Replace"
msgstr "เ_ขียนทับ"

#: ../src/gsearchtool-callbacks.c:1733
msgid "The document name you selected is a folder."
msgstr "ชื่อเอกสารที่คุณเลือกเป็นชื่อโฟลเดอร์"

#: ../src/gsearchtool-callbacks.c:1771
msgid "You may not have write permissions to the document."
msgstr "คุณอาจไม่มีสิทธิ์เขียนลงเอกสารนี้"

#. Translators:  Below are the strings displayed in the 'Date Modified'
#. column of the list view.  The format of this string can vary depending
#. on age of a file.  Please modify the format of the timestamp to match
#. your locale.  For example, to display 24 hour time replace the '%-I'
#. with '%-H' and remove the '%p'.  (See bugzilla report #120434.)
#: ../src/gsearchtool-support.c:452
msgid "today at %-I:%M %p"
msgstr "วันนี้ เวลา %H:%M น."

#: ../src/gsearchtool-support.c:454
msgid "yesterday at %-I:%M %p"
msgstr "เมื่อวาน เวลา %H:%M น."

#: ../src/gsearchtool-support.c:456 ../src/gsearchtool-support.c:458
msgid "%A, %B %-d %Y at %-I:%M:%S %p"
msgstr "%Aที่ %-d %B %Ey เวลา %H:%M:%S น."

#: ../src/gsearchtool-support.c:645
msgid "link (broken)"
msgstr "จุดเชื่อม (ขาด)"

#: ../src/gsearchtool-support.c:649
#, c-format
msgid "link to %s"
msgstr "เชื่อมไปยัง %s"

#. START OF NAUTILUS/EEL FUNCTIONS: USED FOR HANDLING OF DUPLICATE FILENAMES
#. Localizers:
#. * Feel free to leave out the st, nd, rd and th suffix or
#. * make some or all of them match.
#.
#. localizers: tag used to detect the first copy of a file
#: ../src/gsearchtool-support.c:1268
msgid " (copy)"
msgstr " (สำเนา)"

#. localizers: tag used to detect the second copy of a file
#: ../src/gsearchtool-support.c:1270
msgid " (another copy)"
msgstr " (อีกสำเนา)"

#. localizers: tag used to detect the x11th copy of a file
#. localizers: tag used to detect the x12th copy of a file
#. localizers: tag used to detect the x13th copy of a file
#. localizers: tag used to detect the xxth copy of a file
#: ../src/gsearchtool-support.c:1273 ../src/gsearchtool-support.c:1275
#: ../src/gsearchtool-support.c:1277 ../src/gsearchtool-support.c:1287
msgid "th copy)"
msgstr ")"

#. localizers: tag used to detect the x1st copy of a file
#: ../src/gsearchtool-support.c:1280
msgid "st copy)"
msgstr ")"

#. localizers: tag used to detect the x2nd copy of a file
#: ../src/gsearchtool-support.c:1282
msgid "nd copy)"
msgstr ")"

#. localizers: tag used to detect the x3rd copy of a file
#: ../src/gsearchtool-support.c:1284
msgid "rd copy)"
msgstr ")"

#. localizers: appended to first file copy
#: ../src/gsearchtool-support.c:1301
#, c-format
msgid "%s (copy)%s"
msgstr "%s (สำเนา)%s"

#. localizers: appended to second file copy
#: ../src/gsearchtool-support.c:1303
#, c-format
msgid "%s (another copy)%s"
msgstr "%s (อีกสำเนา)%s"

#. localizers: appended to x11th file copy
#. localizers: appended to x12th file copy
#. localizers: appended to x13th file copy
#. localizers: appended to xxth file copy
#: ../src/gsearchtool-support.c:1306 ../src/gsearchtool-support.c:1308
#: ../src/gsearchtool-support.c:1310 ../src/gsearchtool-support.c:1319
#, c-format
msgid "%s (%dth copy)%s"
msgstr "%s (สำเนาที่ %d)%s"

#. localizers: appended to x1st file copy
#: ../src/gsearchtool-support.c:1313
#, c-format
msgid "%s (%dst copy)%s"
msgstr "%s (สำเนาที่ %d)%s"

#. localizers: appended to x2nd file copy
#: ../src/gsearchtool-support.c:1315
#, c-format
msgid "%s (%dnd copy)%s"
msgstr "%s (สำเนาที่ %d)%s"

#. localizers: appended to x3rd file copy
#: ../src/gsearchtool-support.c:1317
#, c-format
msgid "%s (%drd copy)%s"
msgstr "%s (สำเนาที่ %d)%s"

#: ../src/gsearchtool-support.c:1364
msgid " (invalid Unicode)"
msgstr " (รหัสยูนิโค้ดไม่ถูกต้อง)"

#. localizers: opening parentheses to match the "th copy)" string
#: ../src/gsearchtool-support.c:1453
msgid " ("
msgstr " (สำเนาที่"

#. localizers: opening parentheses of the "th copy)" string
#: ../src/gsearchtool-support.c:1461
#, c-format
msgid " (%d"
msgstr " (สำเนาที่ %d"

#: ../libeggsmclient/eggdesktopfile.c:165
#, c-format
msgid "File is not a valid .desktop file"
msgstr "แฟ้มนี้ไม่ใช่แฟ้ม .desktop ที่ใช้ได้"

#: ../libeggsmclient/eggdesktopfile.c:188
#, c-format
msgid "Unrecognized desktop file Version '%s'"
msgstr "ไม่รู้จักแฟ้มเดสก์ท็อปรุ่น '%s'"

#: ../libeggsmclient/eggdesktopfile.c:958
#, c-format
msgid "Starting %s"
msgstr "กำลังเริ่ม %s"

#: ../libeggsmclient/eggdesktopfile.c:1100
#, c-format
msgid "Application does not accept documents on command line"
msgstr "โปรแกรมไม่รับเอกสารในบรรทัดคำสั่ง"

#: ../libeggsmclient/eggdesktopfile.c:1168
#, c-format
msgid "Unrecognized launch option: %d"
msgstr "ไม่รู้จักตัวเลือกของการเรียกโปรแกรม: %d"

#: ../libeggsmclient/eggdesktopfile.c:1373
#, c-format
msgid "Can't pass document URIs to a 'Type=Link' desktop entry"
msgstr "ไม่สามารถส่ง URI ของเอกสารไปยังรายการเดสก์ท็อปที่มี 'Type=Link'"

#: ../libeggsmclient/eggdesktopfile.c:1392
#, c-format
msgid "Not a launchable item"
msgstr "ไม่ใช่รายการที่เรียกทำงานได้"

#: ../libeggsmclient/eggsmclient.c:225
msgid "Disable connection to session manager"
msgstr "ปิดใช้การเชื่อมต่อไปยังโปรแกรมจัดการวาระ"

#: ../libeggsmclient/eggsmclient.c:228
msgid "Specify file containing saved configuration"
msgstr "ระบุแฟ้มที่บันทึกค่าตั้งไว้"

#: ../libeggsmclient/eggsmclient.c:228
msgid "FILE"
msgstr "FILE"

#: ../libeggsmclient/eggsmclient.c:231
msgid "Specify session management ID"
msgstr "ระบุหมายเลขการจัดการวาระ"

#: ../libeggsmclient/eggsmclient.c:231
msgid "ID"
msgstr "ID"

#: ../libeggsmclient/eggsmclient.c:252
msgid "Session management options:"
msgstr "ตัวเลือกเกี่ยวกับการจัดการวาระ:"

#: ../libeggsmclient/eggsmclient.c:253
msgid "Show session management options"
msgstr "แสดงตัวเลือกเกี่ยวกับการจัดการวาระ"

#~ msgid "Select the search option \"Contains the text\""
#~ msgstr "เลือกตัวเลือก \"มีข้อความ\""

#~ msgid "Select the search option \"Date modified less than\""
#~ msgstr "เลือกตัวเลือก \"แก้ไขครั้งล่าสุดภายใน\""

#~ msgid "Select the search option \"Date modified more than\""
#~ msgstr "เลือกตัวเลือก \"แก้ไขครั้งล่าสุดก่อน\""

#~ msgid "Select the search option \"Exclude other filesystems\""
#~ msgstr "เลือกตัวเลือก \"ไม่รวมระบบแฟ้มอื่น\""

#~ msgid "Select the search option \"File is empty\""
#~ msgstr "เลือกตัวเลือก \"แฟ้มเปล่าไม่มีข้อมูล\""

# Once created, hard link is a normal file.
# จุดเชื่อม here unambiguously means symbolic link.
#~ msgid "Select the search option \"Follow symbolic links\""
#~ msgstr "เลือกตัวเลือก \"ติดตามลิงก์สัญลักษณ์\""

#~ msgid "Select the search option \"Name does not contain\""
#~ msgstr "เลือกตัวเลือก \"ชื่อไม่มีคำว่า\""

#~ msgid "Select the search option \"Name matches regular expression\""
#~ msgstr "เลือกตัวเลือก \"ชื่อที่ตรงกับนิพจน์เรกิวลาร์\""

#~ msgid "Select the search option \"Owned by group\""
#~ msgstr "เลือกตัวเลือก \"เป็นของกลุ่ม\""

#~ msgid "Select the search option \"Owned by user\""
#~ msgstr "เลือกตัวเลือก \"เจ้าของคือ\""

#~ msgid "Select the search option \"Owner is unrecognized\""
#~ msgstr "เลือกตัวเลือก \"ไม่รู้จักเจ้าของ\""

#~ msgid "Select the search option \"Show hidden files and folders\""
#~ msgstr "เลือกตัวเลือก \"แสดงแฟ้มสำรอง และแฟ้มที่ถูกซ่อน\""

#~ msgid "Select the search option \"Size at least\""
#~ msgstr "เลือกตัวเลือก \"ขนาดอย่างน้อย\""

#~ msgid "Select the search option \"Size at most\""
#~ msgstr "เลือกตัวเลือก \"ขนาดไม่เกิน\""

#~ msgid ""
#~ "This key determines if the \"Select more options\" section is expanded "
#~ "when the search tool is started."
#~ msgstr "กำหนดให้ส่วน \"เลือกตัวเลือกอื่นๆ\" เปิดไว้เมื่อเริ่มเปิดเครื่องมือค้นหา"

#~ msgid ""
#~ "GConf error:\n"
#~ "  %s"
#~ msgstr ""
#~ "เกิดข้อผิดพลาด GConf:\n"
#~ "  %s"
